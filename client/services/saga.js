import {actionTypes} from "./action-types";
import {takeLatest, takeEvery, put, call, select} from "redux-saga/effects";
import * as api from '../common/api';
import {actions} from "./actions";
import {reducerName} from "./reducer";

function* getInvoices(action) {
    try {
        const customers = yield call(api.getCustomers);
        yield put({type: actionTypes.GET_CUSTOMERS_SUCCESS, payload: customers});
        const products = yield api.getProducts();
        yield put({type: actionTypes.GET_PRODUCTS_SUCCESS, payload: products});
        const invoices = yield api.getInvoices();
        for (const invoice of invoices) {
            invoice.items = yield api.getInvoiceItems(invoice.id);
        }
        yield put({type: actionTypes.GET_INVOICES_SUCCESS, payload: invoices});
    } catch (err) {
        console.error(err);
    }
}

function* saveInvoice({payload: {customer_id, total, discount, items = []}}) {

    try {
        customer_id = +customer_id;
        total = +total;
        discount = +discount;

        const responseInvoice = yield call(api.createInvoice, {customer_id, discount, total});
        console.log(responseInvoice);
        const invoice = {customer_id, total, discount, ...responseInvoice};

        for (const {product_id: _product_id, quantity: _quantity} of items) {
            const product_id = +_product_id, quantity = +_quantity;

            const responseItem = yield call(api.createInvoiceItem, invoice.id, {
                product_id,
                quantity,
                invoice_id: invoice.id
            });
            const item = {product_id, quantity, invoice_id: invoice.id, ...responseItem};
            invoice.items = invoice.items || [];
            invoice.items.push(item);
        }

        yield put(actions.saveInvoice.success(invoice));
    } catch (err) {
        yield put(actions.saveInvoice.failure(err));
    } finally {
        yield put(actions.setInvoiceCreation(false));
    }
}

function* calculateInvoiceTotal(invoice) {
    const productsById = yield select(rootState => rootState[reducerName].get('productsById'));

    const {items = [], discount = 0} = invoice;
    const sum = items.map(({product_id: productId, quantity = 0} = {}) => {
        const {price = 0} = productsById[productId] || {};
        return price * quantity;
    }).reduce((sum, n) => sum + n, 0);

    invoice.total = (sum - (sum*discount/100)).toFixed(2);
}

function* updateInvoice({payload: invoice}) {
    yield calculateInvoiceTotal(invoice);
    let {id, customer_id, total, discount, items = []} = invoice;

    try {
        customer_id = +customer_id;
        total = +total;
        discount = +discount;

        const responseInvoice = yield call(api.updateInvoice, id, {customer_id, discount, total});
        console.log(responseInvoice);
        const invoice = {customer_id, total, discount, ...responseInvoice};

        for (const {id, product_id: _product_id, quantity: _quantity} of items) {
            const product_id = +_product_id, quantity = +_quantity;

            const responseItem = yield call(api.updateInvoiceItem, id, invoice.id, {
                product_id,
                quantity,
                invoice_id: invoice.id
            });
            const item = {product_id, quantity, invoice_id: invoice.id, ...responseItem};
            invoice.items = invoice.items || [];
            invoice.items.push(item);
        }

        yield put(actions.updateInvoice.success(invoice));
    } catch (err) {
        yield put(actions.updateInvoice.failure(err));
    } finally {
        yield put(actions.setInvoiceCreation(false));
    }
}

export default function* () {
    yield takeLatest(actionTypes.GET_INVOICES_REQUEST, getInvoices);
    yield takeEvery(actions.saveInvoice.REQUEST, saveInvoice);
    yield takeEvery(actions.updateInvoice.REQUEST, updateInvoice);
}