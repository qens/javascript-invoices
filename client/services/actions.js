import {actionTypes} from "./action-types";
import {createFormAction} from "redux-form-saga";

export const actions = {
    getCustomers: () => ({
        type: actionTypes.GET_CUSTOMERS_REQUEST
    }),

    getProducts: () => ({
        type: actionTypes.GET_PRODUCTS_REQUEST
    }),

    getInvoices: () => ({
        type: actionTypes.GET_INVOICES_REQUEST
    }),

    setInvoiceCreation: payload => ({type: actionTypes.SET_INVOICE_CREATION, payload}),

    saveInvoice: createFormAction(actionTypes.SAVE_INVOICE),

    updateInvoice: createFormAction(actionTypes.UPDATE_INVOICE),

    updateInvoiceItem: createFormAction(actionTypes.UPDATE_INVOICE_ITEM),
};