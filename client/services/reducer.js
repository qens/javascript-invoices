import {fromJS} from "immutable";
import {actionTypes} from "./action-types";
import keyBy from 'lodash/keyBy';
import {actions} from "./actions";
import invoices from "../modules/invoices/invoices";


export const reducerName = 'reducer';

const initialState = fromJS({
    error: null,
    invoices: [],
    products: [],
    productsById: {},
    customers: [],
    customersById: {},
    loading: false,
    isInvoiceCreation: false
});

function updateInvoice(invoices, payloadInvoice) {
    return invoices.set(invoices.findIndex(invoice => invoice.get('id') === payloadInvoice.id),
        fromJS(payloadInvoice));
}

export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_INVOICES_SUCCESS:
            return state.set('loading', false)
                .set('invoices', fromJS(action.payload));
        case actionTypes.GET_PRODUCTS_SUCCESS:
            return state.set('loading', false)
                .set('products', action.payload)
                .set('productsById', keyBy(action.payload, 'id'));
        case actionTypes.GET_CUSTOMERS_SUCCESS:
            return state.set('loading', false)
                .set('customers', action.payload)
                .set('customersById', keyBy(action.payload, 'id'));

        case actionTypes.GET_INVOICES_REQUEST:
        case actionTypes.GET_PRODUCTS_REQUEST:
        case actionTypes.GET_CUSTOMERS_REQUEST:
            return state.set('loading', true)
                .set('error', null);

        case actionTypes.SET_INVOICE_CREATION:
            return state.set('isInvoiceCreation', action.payload);

        case actions.saveInvoice.SUCCESS:
            return state.update('invoices', invoices => invoices.unshift(fromJS(action.payload)));

        case actions.updateInvoice.SUCCESS:
            return state.update('invoices', invoices => updateInvoice(invoices, action.payload));

        case actions.saveInvoice.FAILURE:
        case actions.updateInvoice.FAILURE:
        case actionTypes.GET_INVOICES_ERROR:
        case actionTypes.GET_PRODUCTS_ERROR:
        case actionTypes.GET_CUSTOMERS_ERROR:
            return state.set('error', `Something happened ${action.payload}`);

        default:
            return state;
    }
}