import React from 'react';
import {Link, Route, Switch} from "react-router-dom";
import {Nav, Navbar, NavItem} from "react-bootstrap";
import Invoices from "./modules/invoices/invoices";

const MainNav = () => <Navbar collapseOnSelect>
    <Navbar.Header>
        <Navbar.Brand>
            <a href="#">Invoice App</a>
        </Navbar.Brand>
        <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
        <Nav>
            <NavItem eventKey={1} componentClass="span">
                <Link to="/">Invoices</Link>
            </NavItem>
            <NavItem eventKey={2} componentClass="span">
                <Link to="/products">Products</Link>
            </NavItem>
            <NavItem eventKey={3} componentClass="span">
                <Link to="/customers">Customers</Link>
            </NavItem>
        </Nav>
    </Navbar.Collapse>
</Navbar>;

export default () => <div>
    <MainNav />
    <Switch>
        <Route exact path="/" component={Invoices} />
    </Switch>
</div>;