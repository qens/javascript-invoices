import {apiClient} from "./api-client";
import {Url} from "./url";


export const BASE_URL = 'http://localhost:8000';

export const getInvoices = () => apiClient.get(new Url(`${BASE_URL}/api/invoices`));
export const createInvoice = (invoice) => apiClient.post(new Url(`${BASE_URL}/api/invoices`), invoice);
export const updateInvoice = (id, invoice) => apiClient.put(new Url(`${BASE_URL}/api/invoices/${id}`), invoice);
export const removeInvoice = (id, invoice) => apiClient.delete(new Url(`${BASE_URL}/api/invoices/${id}`), invoice);

export const getInvoiceItems = (invoiceId) => apiClient.get(new Url(`${BASE_URL}/api/invoices/${invoiceId}/items`));
export const createInvoiceItem = (invoiceId, invoiceItem) => apiClient.post(new Url(`${BASE_URL}/api/invoices/${invoiceId}/items`), invoiceItem);
export const updateInvoiceItem = (id, invoiceId, invoiceItem) => apiClient.put(new Url(`${BASE_URL}/api/invoices/${invoiceId}/items/${id}`), invoiceItem);

export const getProducts = () => apiClient.get(new Url(`${BASE_URL}/api/products`));
export const getCustomers = () => apiClient.get(new Url(`${BASE_URL}/api/customers`));