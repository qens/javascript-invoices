import React from 'react';
import {Button, Col, ListGroup, ListGroupItem, Panel, Row} from "react-bootstrap";
import {Field, FieldArray, Form, getFormValues, reduxForm} from "redux-form";
import {FieldTypes, reduxFormField} from "../../../common/redux-form-field";
import {connect} from "react-redux";
import {reducerName} from "../../../services/reducer";
import {actions} from "../../../services/actions";
import {bindActionCreators} from "redux";
import {required} from "../../../common/validators";

const FORM_NAME = 'CreateInvoice';

const renderProductOption = ({id, name, price}) => `${name} ${price}`;
const renderItems = ({fields, products}) =>
    <ListGroup>
        <ListGroupItem><Button bsClass="btn btn-default center-block" onClick={() => fields.push()}>Add product</Button></ListGroupItem>
        {fields && fields.map((field, index) => <ListGroupItem key={index}><Row>
            <Col sm={6}><Field name={`${field}.product_id`} label="Product" type={FieldTypes.select}
                               component={reduxFormField}
                               required={true}
                               validate={[required]}
                               options={products} labelKey="name" valueKey="id"
                               renderOption={renderProductOption}/></Col>
            <Col sm={6}><Field name={`${field}.quantity`} value={1} label="Quantity" type={FieldTypes.number}
                               component={reduxFormField}/></Col>
        </Row></ListGroupItem>)}
    </ListGroup>;

const CreateInvoice = ({handleSubmit, customers, products, total, dispatch}) => <Form onSubmit={handleSubmit}>
    <Panel footer={<div><Button type="submit" onClick={handleSubmit((values) => dispatch(actions.saveInvoice.request({...values, total})))}>Save</Button></div>}>
        <Row>
            <Col sm={4}>
                <Field name="customer_id" label="Customer" type={FieldTypes.select} component={reduxFormField}
                       required={true}
                       validate={[required]}
                       options={customers} labelKey="name" valueKey="id"/>
            </Col>
            <Col sm={4}>
                <Field name="discount" label="Discount" type={FieldTypes.number} component={reduxFormField}
                       value={0}
                       max={100} min={0}/>
            </Col>
        </Row>
        <Row>
            <Col sm={12}>
                <FieldArray name="items" component={renderItems} {...{products}}/>
            </Col>
        </Row>
        <Row>
            <Col sm={12}>
                <span className="pull-right">{total}</span>
            </Col>
        </Row>
    </Panel></Form>;

const CreateInvoiceForm =  reduxForm({
    form: FORM_NAME,
    enableReinitialize: true
})(CreateInvoice);

export default connect((rootState, props) => {
    const state = rootState[reducerName];
    const formValues = getFormValues(FORM_NAME)(rootState) || {};
    const products = state.get('products');
    const productsById = state.get('productsById');
    const customers = state.get('customers');
    const customersById = state.get('customersById');

    const {items = [], discount = 0} = formValues;
    const sum = items.map(({product_id: productId, quantity = 0} = {}) => {
        const {price = 0} = productsById[productId] || {};
        return price * quantity;
    }).reduce((sum, n) => sum + n, 0);

    const total = (sum - (sum*discount/100)).toFixed(2);

    return {
        products,
        customers,
        total
    }
}, null)(CreateInvoiceForm);
