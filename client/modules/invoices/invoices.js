import React from 'react';
import {Button, Col, Grid, Row} from "react-bootstrap";
import {reducerName} from "../../services/reducer";
import {actions} from "../../services/actions";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import CreateInvoice from './create-invoice/create-invoice';
import {reduxForm} from "redux-form/immutable";
import Invoice from './invoice/invoice';


const FORM_NAME = 'Invoices';

class Invoices extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const {getInvoices} = this.props;
        getInvoices();
    }

    render() {
        const {isInvoiceCreation, setInvoiceCreation, invoices, error} = this.props;

        return <Grid>
            <Row>
                <Col sm={12}><Button onClick={() => setInvoiceCreation(true)}>Create
                    invoice</Button></Col>
            </Row>
            {error ? <Panel header="Error" bsStyle="danger">
                {error}
            </Panel>: null}
            {isInvoiceCreation ? <Row>
                <Col sm={12}>
                    <CreateInvoice/>
                </Col>
            </Row> : null}
            <Row>
                <Col sm={12}>
                    {invoices && invoices.map(invoice => <Invoice key={invoice.get('id')} form={`invoice[${invoice.get('id')}]`}
                                                                  initialValues={invoice.toJS()} invoice={invoice}/>)}
                </Col>
            </Row>
        </Grid>
    }
}

const InvoicesForm = reduxForm({
    form: FORM_NAME,
    enableReinitialize: true
})(Invoices);

const mapStateToProps = (rootState, props) => {
    const state = rootState[reducerName];
    const products = state.get('products');
    const customers = state.get('customers');
    const invoices = state.get('invoices');
    const error = state.get('error');
    const isInvoiceCreation = state.get('isInvoiceCreation');


    console.log(state.toJS());

    return {
        error,
        products,
        customers,
        invoices,
        isInvoiceCreation
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getInvoices: bindActionCreators(() => actions.getInvoices(), dispatch),
        setInvoiceCreation: bindActionCreators(payload => actions.setInvoiceCreation(payload), dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(InvoicesForm);