import React from 'react';
import {Field, FieldArray, Form, formValueSelector, reduxForm} from "redux-form";
import {Button, Col, ListGroup, ListGroupItem, Panel, Row} from "react-bootstrap";
import {required} from "../../../common/validators";
import {FieldTypes, reduxFormField} from "../../../common/redux-form-field";
import {connect} from "react-redux";
import {reducerName} from "../../../services/reducer";
import {getFormValues} from "redux-form/immutable";
import {actions} from "../../../services/actions";

const renderProduct = (productId, productsById) => {
    const product = productsById[productId];
    return product ? <h4>{product.name} {product.price}</h4> : null;
};

const renderItems = ({fields, productsById, selector, handleSubmit}) => {
    return <ListGroup>
        {fields && fields.map((field, index) => <ListGroupItem key={index}><Row>
            <Col sm={6}>{renderProduct(selector(`${field}.product_id`), productsById)}</Col>
            <Col sm={6}><Field name={`${field}.quantity`} value={1} label="Quantity" type={FieldTypes.number}
                               onBlur={handleSubmit(actions.updateInvoice)}
                               component={reduxFormField}/></Col>
        </Row></ListGroupItem>)}
    </ListGroup>;
};

class Invoice extends React.Component {

    render() {
        const {customers, customer, invoice, handleSubmit, total, products, productsById, selector} = this.props;

        return <Form onSubmit={handleSubmit}>
            <Panel>
                <Row>
                    <Col sm={4}>
                        <div><h3>{customer.name}</h3></div>
                    </Col>
                    <Col sm={4}>
                        <Field name="discount" label="Discount" type={FieldTypes.number} component={reduxFormField}
                               onBlur={handleSubmit(actions.updateInvoice)}
                               value={0}
                               max={100} min={0}/>
                    </Col>
                </Row>
                <Row>
                    <Col sm={12}>
                        <FieldArray name="items" component={renderItems} {...{productsById, selector, handleSubmit}}/>
                    </Col>
                </Row>
                <Row>
                    <Col sm={12}>
                        <span className="pull-right">Total: {total}</span>
                    </Col>
                </Row>
            </Panel>
        </Form>
    }
}

const InvoiceForm = reduxForm({
    enableReinitialize: true
})(Invoice);

export default connect((rootState, ownProps) => {
    const state = rootState[reducerName];
    const products = state.get('products');
    const productsById = state.get('productsById');
    const customers = state.get('customers');
    const customersById = state.get('customersById');
    const invoice = ownProps.invoice;
    const total = invoice.get('total');
    const customer = customersById[invoice.get('customer_id')] || {};
    const formSelector = ownProps.selector || formValueSelector(ownProps.form);
    const selector = (...args) => formSelector(rootState, ...args);

    try {
        console.warn((getFormValues(ownProps.form)(rootState)).toJS());
    } catch (err) {
    }

    return {
        products,
        productsById,
        customers,
        customer,
        total,
        selector
    }
})(InvoiceForm);